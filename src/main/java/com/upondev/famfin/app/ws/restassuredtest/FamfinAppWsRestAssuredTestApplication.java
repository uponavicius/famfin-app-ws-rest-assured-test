package com.upondev.famfin.app.ws.restassuredtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamfinAppWsRestAssuredTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(FamfinAppWsRestAssuredTestApplication.class, args);
    }

}
