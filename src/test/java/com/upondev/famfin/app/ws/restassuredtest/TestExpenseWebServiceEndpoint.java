package com.upondev.famfin.app.ws.restassuredtest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestExpenseWebServiceEndpoint {

    private final String CONTEXT_PATH = "/app-ws";
    private final String APPLICATION_JSON = "application/json";
    private static String userId;
    private static String authorizationHeader;
    private static String expenseTypeId;
    private static String expenseId;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;

        TestUsersWebServiceEndpoint testUsersWebServiceEndpoint = new TestUsersWebServiceEndpoint();
        testUsersWebServiceEndpoint.a();
        userId = TestUsersWebServiceEndpoint.userId;
        authorizationHeader = TestUsersWebServiceEndpoint.authorizationHeader;
    }

    /**
     * Test create expense
     */
    @Test
    void a() {
        // Create expense type. To create expense need expenseTypeId from this test.
        Map<String, Object> newExpenseType = new HashMap<>();
        newExpenseType.put("expenseTypeName", "EXPENSE TYPE TEST");

        Response responseCreateExpenseType = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(newExpenseType)
                .pathParam("userId", userId)
                .when()
                .post(CONTEXT_PATH + "/users/{userId}/expenseType")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        expenseTypeId = responseCreateExpenseType.jsonPath().getString("expenseTypeId");

        // Create expense
        Map<String, Object> newExpenses = new HashMap<>();
        newExpenses.put("date", LocalDate.now().minusDays(1).toString());
        newExpenses.put("name", "EXPENSE TEST");
        newExpenses.put("amount", 100.0);

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(newExpenses)
                .pathParam("userId", userId)
                .pathParam("expenseTypeId", expenseTypeId)
                .when()
                .post(CONTEXT_PATH + "/users/{userId}/expenseType/{expenseTypeId}/expense")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        expenseId = response.jsonPath().get("expenseId");
        assertNotNull(expenseId);
        assertEquals(30, expenseId.length());
        assertEquals("EXPENSE TEST", response.jsonPath().getString("name"));
        assertEquals(100, response.jsonPath().getDouble("amount"));

    }

    /**
     * Test update expense
     */
    @Test
    void b() {
        Map<String, Object> updateExpenses = new HashMap<>();
        updateExpenses.put("date", LocalDate.now().toString());
        updateExpenses.put("name", "EXPENSE TEST-UPDATED");
        updateExpenses.put("amount", 100.0);

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(updateExpenses)
                .pathParam("userId", userId)
                .pathParam("expenseTypeId", expenseTypeId)
                .pathParam("expenseId", expenseId)
                .when()
                .put(CONTEXT_PATH + "/users/{userId}/expenseType/{expenseTypeId}/expense/{expenseId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals("EXPENSE TEST-UPDATED", response.jsonPath().getString("name"));

    }

    /**
     * Test get all user expenses
     */
    @Test
    void c() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/expense")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        List<Map<String, String>> expenses = response.jsonPath().getList("name");
        assertNotNull(expenses);
        assertEquals(14, expenses.size());
        assertEquals("EXPENSE TEST-UPDATED", expenses.get(expenses.size() - 1));

    }

    /**
     * Test get one expense
     */
    @Test
    void d() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("expenseTypeId", expenseTypeId)
                .pathParam("expenseId", expenseId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/expenseType/{expenseTypeId}/expense/{expenseId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals("EXPENSE TEST-UPDATED", response.jsonPath().getString("name"));

    }

    /**
     * Test get expenses between two days
     */
    @Test
    void e(){
        Map<String, Object> dates = new HashMap<>();
        dates.put("dateFrom", LocalDate.now().toString());
        dates.put("dateTo", LocalDate.now().toString());

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .body(dates)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/expense")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        List<Map<String, String>> expense = response.jsonPath().getList("name");
        assertEquals(1, expense.size());
        assertEquals("EXPENSE TEST-UPDATED", expense.get(0));
    }

    /**
     * Test get expense between two days ant type
     */
    @Test
    void f(){
        Map<String, Object> dates = new HashMap<>();
        dates.put("dateFrom", LocalDate.now().toString());
        dates.put("dateTo", LocalDate.now().toString());

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("expenseTypeId", expenseTypeId)
                .body(dates)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/expenseType/{expenseTypeId}/expense")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        List<Map<String, String>> expenses = response.jsonPath().getList("name");
        assertEquals(1, expenses.size());
        assertEquals("EXPENSE TEST-UPDATED", expenses.get(0));

    }

    /**
     * Test delete expense and delete expense type
     */
    @Test
    void g() {
        // Delete expense
        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("expenseTypeId", expenseTypeId)
                .pathParam("expenseId", expenseId)
                .when()
                .delete(CONTEXT_PATH + "/users/{userId}/expenseType/{expenseTypeId}/expense/{expenseId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        String operationResult = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationResult);

        // Delete expense type
        Response responseDeleteExpenseType = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("expenseTypeId", expenseTypeId)
                .when()
                .delete(CONTEXT_PATH + "/users/{userId}/expenseType/{expenseTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String operationDeleteExpenseType = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationDeleteExpenseType);

    }

}
