package com.upondev.famfin.app.ws.restassuredtest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class TestStatisticWebServiceEndpoint {

    private final String CONTEXT_PATH = "/app-ws";
    private final String APPLICATION_JSON = "application/json";
    private static String userId;
    private static String authorizationHeader;
    private static String incomeTypeId;
    private static String incomeId;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;

        TestUsersWebServiceEndpoint testUsersWebServiceEndpoint = new TestUsersWebServiceEndpoint();
        testUsersWebServiceEndpoint.a();
        userId = TestUsersWebServiceEndpoint.userId;
        authorizationHeader = TestUsersWebServiceEndpoint.authorizationHeader;
    }

    /**
     * Test get shared statistic
     */
    @Test
    void testSharedStatistic() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/sharedStatistic")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        double currentMonthIncomes = response.jsonPath().getDouble("currentMonthIncomes");
        assertEquals(4000.0, currentMonthIncomes);

        double currentMonthExpenses = response.jsonPath().getDouble("currentMonthExpenses");
        assertEquals(2450.0, currentMonthExpenses);

        double currentMonthBalance = response.jsonPath().getDouble("currentMonthBalance");
        assertEquals(1550, currentMonthBalance);

        double allTimeBalance = response.jsonPath().getDouble("allTimeBalance");
        assertEquals(4000, allTimeBalance);
    }

    /**
     * Test get incomes statistic between two days by types
     */
    @Test
    void testIncomesStatisticBetweenTwoDaysByTypes() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(datesFromAndTo())
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/incomesStatistic")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        List<Map<String, Map<String, Double>>> incomesStatisticsList = response.jsonPath().getList("incomesStatistic");

        double husbandIncomes = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) incomesStatisticsList.get(0)).get(0).get("Husband Incomes")));
        assertEquals(4000.0, husbandIncomes);

        double wifeIncomes = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) incomesStatisticsList.get(0)).get(0).get("Wife Incomes")));
        assertEquals(4000.0, wifeIncomes);
    }

    /**
     * Test get expenses statistic between two days by types
     */
    @Test
    void testExpensesStatisticBetweenTwoDaysByTypes() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(datesFromAndTo())
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/expenseStatistic")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        List<Map<String, Map<String, Double>>> expensesStatisticsList = response.jsonPath().getList("expenseStatistic");

        double carMaintenance = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesStatisticsList.get(0)).get(0).get("Car Maintenance")));
        assertEquals(800.0, carMaintenance);

        double clothingAndFootwear = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesStatisticsList.get(0)).get(0).get("Clothing and Footwear")));
        assertEquals(700.0, clothingAndFootwear);

        double credit = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesStatisticsList.get(0)).get(0).get("Credit")));
        assertEquals(1200.0, credit);

        double food = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesStatisticsList.get(0)).get(0).get("Food")));
        assertEquals(300.0, food);

        double fuel = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesStatisticsList.get(0)).get(0).get("Fuel")));
        assertEquals(200.0, fuel);

        double health = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesStatisticsList.get(0)).get(0).get("Health")));
        assertEquals(800.0, health);
    }

    /**
     * Test get incomes by month between two days
     */
    @Test
    void testIncomesByMonthBetweenTwoDays() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(datesFromAndTo())
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/incomesByMonth")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        List<Map<String, Map<String, Double>>> incomesByMonth = response.jsonPath().getList("incomesByMonth");

        double y2020m04 = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) incomesByMonth.get(0)).get(0).get("2020-04")));
        assertEquals(4000.0, y2020m04);

        double y2020m05 = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) incomesByMonth.get(0)).get(0).get("2020-05")));
        assertEquals(4000.0, y2020m05);
    }

    /**
     * Test get expenses by month between two days
     */
    @Test
    void testExpensesByMonthBetweenTwoDays() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(datesFromAndTo())
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/expensesByMonth")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        List<Map<String, Map<String, Double>>> expensesByMonth = response.jsonPath().getList("expensesByMonth");

        double y2020m04 = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesByMonth.get(0)).get(0).get("2020-04")));
        assertEquals(1550.0, y2020m04);

        double y2020m05 = Double.parseDouble(String.valueOf(((ArrayList<Map<String, Double>>) expensesByMonth.get(0)).get(0).get("2020-05")));
        assertEquals(2450.0, y2020m05);
    }

    private Map<String, Object> datesFromAndTo() {
        Map<String, Object> dates = new HashMap<>();
        dates.put("dateFrom", LocalDate.now().minusMonths(2).toString());
        dates.put("dateTo", LocalDate.now().plusMonths(1).toString());
        return dates;
    }

}

