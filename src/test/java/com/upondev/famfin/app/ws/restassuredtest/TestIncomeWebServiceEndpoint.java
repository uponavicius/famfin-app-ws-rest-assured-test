package com.upondev.famfin.app.ws.restassuredtest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestIncomeWebServiceEndpoint {

    private final String CONTEXT_PATH = "/app-ws";
    private final String APPLICATION_JSON = "application/json";
    private static String userId;
    private static String authorizationHeader;
    private static String incomeTypeId;
    private static String incomeId;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;

        TestUsersWebServiceEndpoint testUsersWebServiceEndpoint = new TestUsersWebServiceEndpoint();
        testUsersWebServiceEndpoint.a();
        userId = TestUsersWebServiceEndpoint.userId;
        authorizationHeader = TestUsersWebServiceEndpoint.authorizationHeader;
    }

    /**
     * Test create income
     */
    @Test
    void a() {
        //create Income type. To create Income need incomeTypeId from this test.
        Map<String, Object> newIncomeType = new HashMap<>();
        newIncomeType.put("incomeTypeName", "Type From INCOME TEST");

        Response responseCreateIncomeType = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(newIncomeType)
                .pathParam("userId", userId)
                .when()
                .post(CONTEXT_PATH + "/users/{userId}/incomeType")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        incomeTypeId = responseCreateIncomeType.jsonPath().getString("incomeTypeId");

        // Create income
        Map<String, Object> newIncome = new HashMap<>();
        newIncome.put("date", LocalDate.now().minusDays(1).toString());
        newIncome.put("name", "INCOME TEST");
        newIncome.put("amount", 1000);

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(newIncome)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .when()
                .post(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}/income")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        incomeId = response.jsonPath().getString("incomeId");
        assertNotNull(incomeId);
        assertEquals(30, incomeId.length());
        assertEquals("INCOME TEST", response.jsonPath().getString("name"));
        assertEquals(1000, response.jsonPath().getDouble("amount"));


    }

    /**
     * Test update income
     */
    @Test
    void b() {
        Map<String, Object> updateIncome = new HashMap<>();
        updateIncome.put("date", LocalDate.now().toString());
        updateIncome.put("name", "INCOME TEST-UPDATED");
        updateIncome.put("amount", 100);

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(updateIncome)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .pathParam("incomeId", incomeId)
                .when()
                .put(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}/income/{incomeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals("INCOME TEST-UPDATED", response.jsonPath().getString("name"));
        assertEquals(100, response.jsonPath().getDouble("amount"));
    }

    /**
     * Test gel all userincomes
     */
    @Test
    void c() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/income")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        List<Map<String, String>> incomes = response.jsonPath().getList("name");
        assertNotNull(incomes);
        assertEquals(5, incomes.size());
        assertEquals("INCOME TEST-UPDATED", incomes.get(incomes.size() - 1));
    }

    /**
     * Test get one income
     */
    @Test
    void d() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .pathParam("incomeId", incomeId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}/income/{incomeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        assertEquals("INCOME TEST-UPDATED", response.jsonPath().getString("name"));
    }

    /**
     * Test get incomes between two days
     */
    @Test
    void e() {
        Map<String, Object> dates = new HashMap<>();
        dates.put("dateFrom", LocalDate.now().toString());
        dates.put("dateTo", LocalDate.now().toString());

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .body(dates)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/income")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        List<Map<String, String>> incomes = response.jsonPath().getList("name");
        assertEquals(1, incomes.size());
        assertEquals("INCOME TEST-UPDATED", incomes.get(0));
    }

    /**
     * Test get incomes between two days ant type
     */
    @Test
    void f() {
        Map<String, Object> dates = new HashMap<>();
        dates.put("dateFrom", LocalDate.now().toString());
        dates.put("dateTo", LocalDate.now().toString());

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .body(dates)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}/income")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        List<Map<String, String>> incomes = response.jsonPath().getList("name");
        assertEquals(1, incomes.size());
        assertEquals("INCOME TEST-UPDATED", incomes.get(0));

    }

    /**
     * Test delete income and income type
     */
    @Test
    void g() {
        // Delete income
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .pathParam("incomeId", incomeId)
                .when()
                .delete(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}/income/{incomeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        assertNotNull(response);
        String operationResult = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationResult);

        // Delete income type
        Response responseDeleteIncomeType = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .when()
                .delete(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();
        String operationResultDeleteIncomeType = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationResultDeleteIncomeType);

    }
}
