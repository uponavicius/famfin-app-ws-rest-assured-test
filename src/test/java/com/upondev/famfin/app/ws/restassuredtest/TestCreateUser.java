package com.upondev.famfin.app.ws.restassuredtest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class TestCreateUser {

    private final String CONTEXT_PATH = "/app-ws";
    private final String APPLICATION_JSON = "application/json";

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    /**
     * Test create user
     * ATTENTION! Use real email address, because  will receive an email to confirm the email.
     */
    @Test
    void testCreateUser() {
        // Create JSON payload
        Map<String, Object> userDetails = new HashMap<>();
        userDetails.put("firstName", "Vladas");
        userDetails.put("lastName", "Uponavicius");
        userDetails.put("email", "uponavicius@gmail.com");
        userDetails.put("password", "123444");

        Response response = given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(userDetails)
                .when()
                .post(CONTEXT_PATH + "/users")
                .then()
                .statusCode(200)
                .contentType("application/json")
                .extract()
                .response();

        String userId = response.jsonPath().getString("userId");
        assertNotNull(userId);
        assertTrue(userId.length() == 30);
    }

}
