package com.upondev.famfin.app.ws.restassuredtest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestIncomeTypeWebServiceEndpoint {

    private final String CONTEXT_PATH = "/app-ws";
    private final String APPLICATION_JSON = "application/json";
    private static String userId;
    private static String authorizationHeader;
    static String incomeTypeId;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;

        TestUsersWebServiceEndpoint testUsersWebServiceEndpoint = new TestUsersWebServiceEndpoint();
        testUsersWebServiceEndpoint.a();
        userId = TestUsersWebServiceEndpoint.userId;
        authorizationHeader = TestUsersWebServiceEndpoint.authorizationHeader;
    }

    /**
     * Test create income type
     */
    @Test
    void a() {
        // Create JSON payload
        Map<String, Object> newIncomeType = new HashMap<>();
        newIncomeType.put("incomeTypeName", "INCOME TYPE TEST");

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(newIncomeType)
                .pathParam("userId", userId)
                .when()
                .post(CONTEXT_PATH + "/users/{userId}/incomeType")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        incomeTypeId = response.jsonPath().getString("incomeTypeId");

        assertNotNull(incomeTypeId);
        assertTrue(incomeTypeId.length() == 30);

        String incomeTypeName = response.jsonPath().getString("incomeTypeName");
        assertEquals("INCOME TYPE TEST", incomeTypeName);
    }

    /**
     * Test update income type
     */
    @Test
    void b() {
        Map<String, Object> updateIncomeType = new HashMap<>();
        updateIncomeType.put("incomeTypeName", "INCOME TYPE TEST-UPDATED");

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(updateIncomeType)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .when()
                .put(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String incomeTypeId = response.jsonPath().getString("incomeTypeId");
        String incomeTypeName = response.jsonPath().getString("incomeTypeName");

        assertNotNull(incomeTypeId);
        assertNotNull(incomeTypeName);
        assertTrue(incomeTypeId.length() == 30);
        assertTrue(incomeTypeId.equals(this.incomeTypeId));
        assertEquals("INCOME TYPE TEST-UPDATED", incomeTypeName);
    }

    /**
     * Test get all user income types
     */
    @Test
    void c() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", this.userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/incomeType")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        List<Map<String, String>> incomesTypes = response.jsonPath().getList("incomeTypeName");
        assertNotNull(incomesTypes);
        assertEquals(6, incomesTypes.size());
        assertEquals("Husband Incomes", incomesTypes.get(0));
    }

    /**
     * Test get one income type
     */
    @Test
    void d(){
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", this.userId)
                .pathParam("incomeTypeId", this.incomeTypeId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String incomeTypeName = response.jsonPath().getString("incomeTypeName");
        assertNotNull(incomeTypeName);
        assertEquals("INCOME TYPE TEST-UPDATED", incomeTypeName);
    }

    /**
     * Test delete income type
     */
    @Test
    void e() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", incomeTypeId)
                .when()
                .delete(CONTEXT_PATH + "/users/{userId}/incomeType/{incomeTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String operationResult = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationResult);
    }

}
