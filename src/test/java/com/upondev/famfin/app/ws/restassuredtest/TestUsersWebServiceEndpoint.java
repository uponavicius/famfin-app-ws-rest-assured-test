package com.upondev.famfin.app.ws.restassuredtest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestUsersWebServiceEndpoint {

    private final String CONTEXT_PATH = "/app-ws";
    /**
     * ATTENTION! EMAIL - use the email address you used to create the user.
     */
    private final String EMAIL = "uponavicius@gmail.com";
    private final String APPLICATION_JSON = "application/json";
    static String userId;
    static String authorizationHeader;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    /**
     * Test user login
     * ATTENTION! Please confirm your email first.
     * ATTENTION! password - use the password you used to create the user.
     */
    @Test
    void a() {
        // Create JSON payload
        Map<String, Object> loginDetails = new HashMap<>();
        loginDetails.put("email", EMAIL);
        loginDetails.put("password", "123444");

        Response response = given()
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(loginDetails)
                .when()
                .get(CONTEXT_PATH + "/users/login")
                .then()
                .statusCode(200)
                .extract()
                .response();

        authorizationHeader = response.header("Authorization");
        // from app-ws com.upondev.app.ws.security;
        // res.addHeader("UserId", userDto.getUserId()); //  In header add user public userId;
        userId = response.header("UserID");

        assertNotNull(authorizationHeader);
        assertNotNull(userId);
    }

    /**
     * Test get user details
     */
    @Test
    void b() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .pathParam("userId", userId)
                .accept(APPLICATION_JSON)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String publicUserId = response.jsonPath().getString("userId");
        String userEmail = response.jsonPath().getString("email");
        String firstName = response.jsonPath().getString("firstName");
        String lastName = response.jsonPath().getString("lastName");

        assertNotNull(publicUserId);
        assertNotNull(userEmail);
        assertNotNull(firstName);
        assertNotNull(lastName);

        assertEquals(EMAIL, userEmail);
    }

    /**
     * Test update user details
     */
    @Test
    void c() {
        Map<String, Object> updateUserDetails = new HashMap<>();
        updateUserDetails.put("firstName", "Vladas");
        updateUserDetails.put("lastName", "Uponaviciussssssssss");

        Response response = given()
                .header("Authorization", authorizationHeader)
                .pathParam("userId", userId)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(updateUserDetails)
                .when()
                .put(CONTEXT_PATH + "/users/{userId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String firstName = response.jsonPath().getString("firstName");
        String lastName = response.jsonPath().getString("lastName");

        assertEquals("Vladas", firstName);
        assertEquals("Uponaviciussssssssss", lastName);
    }

    /**
     * Test delete user details
     */
    @Test
    @Disabled
    void d() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .pathParam("userId", userId)
                .accept(APPLICATION_JSON)
                .when()
                .delete(CONTEXT_PATH + "/users/{userId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String operationResult = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationResult);
    }

}
