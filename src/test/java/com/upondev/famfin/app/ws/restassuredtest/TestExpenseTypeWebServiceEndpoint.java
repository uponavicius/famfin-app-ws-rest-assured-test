package com.upondev.famfin.app.ws.restassuredtest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestExpenseTypeWebServiceEndpoint {

    private final String CONTEXT_PATH = "/app-ws";
    private final String APPLICATION_JSON = "application/json";
    private static String userId;
    private static String authorizationHeader;
    private static String expenseTypeId;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;

        TestUsersWebServiceEndpoint testUsersWebServiceEndpoint = new TestUsersWebServiceEndpoint();
        testUsersWebServiceEndpoint.a();
        userId = TestUsersWebServiceEndpoint.userId;
        authorizationHeader = TestUsersWebServiceEndpoint.authorizationHeader;
    }

    /**
     * Test create expense type
     */
    @Test
    void a() {
        Map<String, Object> newExpenseType = new HashMap<>();
        newExpenseType.put("expenseTypeName", "EXPENSE TYPE TEST");

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(newExpenseType)
                .pathParam("userId", userId)
                .when()
                .post(CONTEXT_PATH + "/users/{userId}/expenseType")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        expenseTypeId = response.jsonPath().getString("expenseTypeId");
        assertNotNull(expenseTypeId);
        assertTrue(expenseTypeId.length() == 30);

        String expenseTypeName = response.jsonPath().getString("expenseTypeName");
        assertEquals("EXPENSE TYPE TEST", expenseTypeName);

    }

    /**
     * Test update expense type
     */
    @Test
    void b() {
        Map<String, Object> updateExpenseType = new HashMap<>();
        updateExpenseType.put("expenseTypeName", "EXPENSE TYPE TEST-UPDATED");

        Response response = given()
                .header("Authorization", authorizationHeader)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .body(updateExpenseType)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", expenseTypeId)
                .when()
                .put(CONTEXT_PATH + "/users/{userId}/expenseType/{incomeTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String expenseTypeId = response.jsonPath().getString("expenseTypeId");
        String expenseTypeName = response.jsonPath().getString("expenseTypeName");

        assertNotNull(expenseTypeId);
        assertNotNull(expenseTypeName);
        assertTrue(expenseTypeId.length() == 30);
        assertTrue(expenseTypeId.equals(this.expenseTypeId));
        assertEquals("EXPENSE TYPE TEST-UPDATED", expenseTypeName);
    }

    /**
     * Test get all user expense types
     */
    @Test
    void c() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .when()
                .get(CONTEXT_PATH + "/users/{userId}/expenseType")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        List<Map<String, String>> expenseTypes = response.jsonPath().getList("expenseTypeName");
        assertNotNull(expenseTypes);
        assertEquals(20, expenseTypes.size());
        assertEquals("Car Maintenance", expenseTypes.get(0));
    }

    /**
     * Test get one expense type
     */
    @Test
    void d() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("incomeTypeId", expenseTypeId)
                .get(CONTEXT_PATH + "/users/{userId}/expenseType/{incomeTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String expenseTypeName = response.jsonPath().getString("expenseTypeName");
        assertNotNull(expenseTypeName);
        assertEquals("EXPENSE TYPE TEST-UPDATED", expenseTypeName);
    }


    /**
     * Test delete expense type
     */
    @Test
    void e() {
        Response response = given()
                .header("Authorization", authorizationHeader)
                .accept(APPLICATION_JSON)
                .pathParam("userId", userId)
                .pathParam("expenseTypeId", expenseTypeId)
                .when()
                .delete(CONTEXT_PATH + "/users/{userId}/expenseType/{expenseTypeId}")
                .then()
                .statusCode(200)
                .contentType(APPLICATION_JSON)
                .extract()
                .response();

        String operationResult = response.jsonPath().getString("operationResult");
        assertEquals("SUCCESS", operationResult);
    }

}
