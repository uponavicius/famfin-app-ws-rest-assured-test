# famfin-app-ws-rest-assured-test
## **Introduction**

Welcome to the **Family Finances application web service rest assured tests app**

This Spring Boot project was created with Java 1.8

### First run FamFin-app-ws 
FamFin-app-ws project https://gitlab.com/uponavicius/famfin-app-ws

### Run Family Finances application web service rest assured tests

1. Download project famfin-app-ws-rest-assured-test-master.zip file.
2. Unpack to your desired location.
3. Open project favorite IDEA (Example Intellij IDEA ultimate).
4. Go to src\test\java\com\upondev\famfin\app\ws\restassuredtest and open TestCreateUser class.
5. Go to testCreateUser() method and change email address uponavicius@gmail.com to your email address. ATTENTION! Use real email address, because  will receive an email to confirm the email.
6. Run TestCreateUser{} class.
7. Go to your email and open email with subject **One last step to complete your registration with FamFinApp** and press link.
8. Open TestUsersWebServiceEndpoint class and change email address in **private final String EMAIL = "uponavicius@gmail.com";** Use the email address you used to register in the app.
9. Run TestUsersWebServiceEndpoint class.
10. Run TestExpenseTypeWebServiceEndpoint class.
11. Run TestExpenseWebServiceEndpoint class.
12. Run TestIncomeTypeWebServiceEndpoint class.
13. TestIncomeWebServiceEndpoint class.
14. Run TestStatisticWebServiceEndpoint class.

Done! You run all Rest-assured tests.